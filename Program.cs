﻿using System;
using System.Collections.Generic;
using System.Linq.Expressions;

namespace Task7
{
	class Program
	{
		static void Main(string[] args)
		{
			List<Animal> animals = new List<Animal>();

			HouseCat cat = new HouseCat("Main Coone", 40, "Green", 'F', 0.1, "Garfield");
			CatAnimal tiger = new CatAnimal("Tiger", 4000, "Yellow", 'M', 2.0);
			DogAnimal wolf = new DogAnimal("Gray Wolf", 100, "Gray", 9);
			DogAnimal dog = new DogAnimal("Dachshund", 40, "Brown", 4);

			animals.Add(cat);
			animals.Add(tiger);
			animals.Add(wolf);
			animals.Add(dog);

			foreach(Animal allAnimals in animals)
            {
                Console.WriteLine(allAnimals.ToString());
            }

            Console.WriteLine(); //make it look nicer in the console
			cat.Meow();
			tiger.Meow();
			dog.Sleep();
			wolf.Eat();
		}
	}
}
