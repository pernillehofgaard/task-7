﻿using System.ComponentModel;

namespace Task7
{
    public abstract class Animal
    {
        public string Species { get; set; }
        public int Height { get; set; }
        public string EyeColor { get; set; }

        public Animal(string species, int height, string eyeColor)
        {
            Species = species;
            Height = height;
            EyeColor = eyeColor;
        }

        public override string ToString()
        {
            return "Species: " + this.Species + ", Height: " + this.Height + "cm, eye color: " + this.EyeColor;
        }

        public abstract void Eat();
        public abstract void Sleep();
    }
}
