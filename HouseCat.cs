﻿using System.ComponentModel;

namespace Task7
{
    public class HouseCat : CatAnimal
    {
        public string Name { get; set; }

        public HouseCat(string species, int height, string eyeColor, char Gender, double HoursSinceLastMeal, string name)
            :base(species, height, eyeColor, Gender, HoursSinceLastMeal)
        {
            Name = name;
            this.HoursSinceLastMeal = HoursSinceLastMeal;
        }

        public override void Meow()
        {
            string wichGender = "";
            if (Gender.Equals('F'))
            {
                wichGender = "she";
            }
            else
            {
                wichGender = "he";
            }
            // Change this.Species to this.Name
            System.Console.WriteLine(this.Name + " is meowing because it has been " + this.HoursSinceLastMeal + " hours since " + wichGender + " ate");
        }
    }


}
